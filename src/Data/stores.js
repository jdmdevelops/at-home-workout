import { writable } from "svelte/store";

export let sets = writable(0);
export const cap = writable(13);
export let temp = writable(0);
export let maxSets = writable(0);
export let timeLeft = writable(0);
