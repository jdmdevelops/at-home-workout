export const arr = [
    {
        title: "Push",
        exercises: [
            ["Push ups", "Incline push ups"],
            ["Pike push ups", "Vertical push ups"],
            ["Standing press w/ milk jug or dumbells"],
            [
                "Lateral raises w/ milk jug or dumbbells",
                "Doorway lateral Raises"
            ],
        ],
        set: 0
    },
    {
        title: "Pull",
        exercises: [
            ["Doorway pullups", "Tree branch pull ups", "Banded pulldowns"],
            ["Table/desk inverted row", "Dumbbell row"],
            ["Rear delt flyes w/ dumbbells or milk jug"],
            ["Upright row w/ backpack or milk jug"]
        ],
        set: 0
    },
    {
        title: "Legs",
        exercises: [
            ["Walking lunges w/ weighted backpack or dumbbells"],
            ["Bulgarian split squat"],
            ["Single leg hip thrust"],
            ["Nordic ham curl", "Pistol squat"]
        ],
        set: 0
    },
    {
        title: "Isolation",
        exercises: [
            ["Bicep curls w/ milk jug or dumbbell", "Weightless flex curls"],
            [
                "Bodweight skullcrushers against a table top",
                "Close grip pushups",
                "Dumbbell floor skullcrushers"
            ],
            ["Bicycle crunch", "Reverse crunch"],
            ["Standing calf raise w/ backpack, milk jug or dumbbell"]
        ],
        set: 0
    }
  ];