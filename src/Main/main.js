import App from "../Components/App.svelte";
import "sanitize.css";

const app = new App({
  target: document.querySelector("#app"),
});

export default app;
